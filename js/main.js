let base_url = 'http://image.tmdb.org/t/p'
let api_key = '82935b2db4a1fa759c1150e4e7193fe1'
var curr_page = 1;
var curr_name = '';
var last_name = ''
var total_pages = 1000;

async function fetchData(url) {
    const response = await fetch(url)
        .then(function(response) {
            if (!response.ok) {
                throw Error(response.statusText);
            }
            return response;
        });
    const res = await response.json();
    return res;
}

function loading_effect(method) {
    if (method === 'on') {
        $.Toast.showToast({
            // toast message
            "title": "Loading.....",
            // "success", "none", "error"
            "icon": "loading",
            "duration": -1
        });
    } else if (method === 'off') {
        $.Toast.hideToast();
    }

}

async function load_trending(page = 1) {
    curr_page = 1;
    loading_effect('on');
    url = `https://api.themoviedb.org/3/trending/all/week?api_key=${api_key}&page=${page}`;
    res = await fetchData(url)
    total_pages = res.total_pages;
    $('#content').empty();
    for (var movie of res.results) {
        $('#content').append(`
            <div class="list-group-item mt-4">
                <div class="row">
                    <div class="col-4">
                        <img src="${base_url + '/w300' + movie.poster_path}" alt="">
                    </div>
                    <div class="col-6">
                        <h4>${movie.title != undefined?movie.title:movie.name}</h4>
                        <p>${movie.overview}</p>
                        <p>Rated: ${movie.vote_average}</p>
                        <p>Release Day: ${movie.release_date}</p>
                        <button class="btn btn-primary" onclick="show_movie_detail(${movie.id})" role="button">Details</button>
                    </div>
                </div>
            </div>
        `)
    }
    loading_effect('off');
    // init bootpag
    $('#pagination').bootpag({ total: total_pages, maxVisible: 5 });
}


function notFound(total_pages) {
    if (total_pages <= 0) {
        $.notify({
            // options
            message: `No results found for "${curr_name}"`
        }, {
            // settings
            type: 'danger',
            allow_dismiss: true,
            newest_on_top: false,
            showProgressbar: false,
            placement: {
                from: "top",
                align: "left"
            },
            offset: 20,
            spacing: 10,
            z_index: 1031,
            delay: 5000,
            timer: 1000,
            url_target: '_blank',
            mouse_over: null,
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            }
        });
        loading_effect('off');
        curr_name = last_name;
        return false;
    }
    return true;
}

async function load_name(page = 1) {
    loading_effect('on');
    url = `https://api.themoviedb.org/3/search/movie?api_key=${api_key}&language=en-US&query=${encodeURI(curr_name)}&page=${page}&include_adult=false`
    res = await fetchData(url)
    total_pages = res.total_pages;
    if (!notFound(total_pages)) return;
    $('#content').empty();
    for (var movie of res.results) {
        $('#content').append(`
        <div class="list-group-item mt-4">
            <div class="row">
                <div class="col-4">
                    <img src="${base_url + '/w300' + movie.poster_path}" alt="">
                </div>
                <div class="col-6">
                    <h4>${movie.title != undefined?movie.title:movie.name}</h4>
                    <p>${movie.overview}</p>
                    <p>Rated: ${movie.vote_average}</p>
                    <p>Release Day: ${movie.release_date}</p>
                    <button class="btn btn-primary" onclick="show_movie_detail(${movie.id})" role="button">Details</button>
                </div>
            </div>
        </div>
    `)
    }
    loading_effect('off');
    $('#pagination').bootpag({ total: total_pages, maxVisible: 5 });
    curr_page = 2;
}

async function load_actor(page = 1) {
    loading_effect('on');
    url = `https://api.themoviedb.org/3/search/person?api_key=${api_key}&language=en-US&query=${encodeURI(curr_name)}&page=${page}&include_adult=false`
    res = await fetchData(url)
    total_pages = res.total_pages;
    if (!notFound(total_pages)) return;
    $('#content').empty();
    for (var person of res.results) {
        for (var movie of person['known_for']) {
            $('#content').append(`
            <div class="list-group-item mt-4">
                <div class="row">
                    <div class="col-4">
                        <img src="${base_url + '/w300' + movie.poster_path}" alt="">
                    </div>
                    <div class="col-6">
                        <h4>${movie.title != undefined?movie.title:movie.name}</h4>
                        <p>${movie.overview}</p>
                        <p>Rated: ${movie.vote_average}</p>
                        <p>Release Day: ${movie.release_date}</p>
                        <button class="btn btn-primary" onclick="show_movie_detail(${movie.id})" role="button">Details</button>
                    </div>
                </div>
            </div>
        `)
        }
    }
    loading_effect('off');
    $('#pagination').bootpag({ total: total_pages, maxVisible: 5 });
    curr_page = 3;
}

function search() {
    if ($("#search-content").val() == '') {
        document.getElementById("search-content").className = document.getElementById("search-content").className + " error";
        return false;
    }
    if ($("#search-type").val() === 'name') {
        last_name = curr_name;
        curr_name = $("#search-content").val();
        load_name();
    } else if (($("#search-type").val() === 'actor')) {
        last_name = curr_name;
        curr_name = $("#search-content").val();
        load_actor();
    }
    document.getElementById("search-content").className = document.getElementById("search-content").className.replace(" error", "");

}

$("#search-btn").on('click', function() {
    search();
});

async function show_person_detail(id) {
    loading_effect('on');

    url = `http://api.themoviedb.org/3/person/${id}?api_key=${api_key}`;
    person = await fetchData(url);

    $('#personModal').empty();
    $('#personModal').append(`
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">${person.name}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-2">
                            <img class="mx-auto d-block" src="${base_url + '/w185' + person.profile_path}">
                        </div>
                        <div class="col-8 offset-1">
                            <p>Name: ${person.name}</p>
                            <p>Birthday: ${person.birthday}</p>
                            <p>Biography: ${person.biography}</p>
                        </div>
                    </div>
                    <p>Joined movies: </p>
                    <div class="table-responsive">
                    <table id="joined-movies" class="table">
                    <thead>
                        <tr>
                        <th>Name</th>
                        <th>Poster</th>
                        <th>Character</th>
                        <th>Release Day</th>
                        </tr>
                    </thead>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
        </div>
    `);

    url = `http://api.themoviedb.org/3/person/${id}/movie_credits?api_key=${api_key}`;
    // movie_credits = await fetchData(url);
    $('#joined-movies').DataTable({
        "ajax": {
            "url": url,
            "dataSrc": "cast"
        },
        "scrollY": "250px",
        "scrollCollapse": true,
        "columns": [
            { data: "title" },
            {
                "data": "poster_path",
                "render": function(data, type, row) {
                    if (data === null) {
                        return '';
                    }
                    return `<img class="mx-auto d-block" src="${base_url + '/w185' + data}">`;
                }
            },
            { data: "character" },
            {
                "data": "release_date",
                "render": function(data, type, row) {
                    if (data == null) {
                        return '';
                    }
                    return data;
                }
            }
        ]
    });

    loading_effect('off');

    $('#personModal').modal('show');
}


async function show_movie_detail(id) {
    loading_effect('on');
    url = `http://api.themoviedb.org/3/movie/${id}?api_key=${api_key}`;
    movie = await fetchData(url);
    url = `http://api.themoviedb.org/3/movie/${id}/credits?api_key=${api_key}`;
    credits = await fetchData(url);
    url = `http://api.themoviedb.org/3/movie/${id}/reviews?api_key=${api_key}`;
    reviews = await fetchData(url);

    var htmlReviews = [];
    reviews.results.forEach(function(entry) {
        htmlReviews.push(`
        \n
                <h6 class="mt-4">${entry.author}</h6>
                <div class="list-group-item list-group-item-action flex-column align-items-start comment">
                    <small class="mb-1">${entry.content}</small>
                    <br>
                    <a href="${entry.url}"><small>more</small></a>
                </div>
        \n
            `);
    })

    var directors = [];
    credits.crew.forEach(function(entry) {
        if (entry.job === 'Director') {
            directors.push(`<a href="#" onclick="show_person_detail(${entry.id})">${entry.name}</a>`);
        }
    })
    var casts = [];
    credits.cast.forEach(function(entry) {
        casts.push(`<a href="#" onclick="show_person_detail(${entry.id})">${entry.name}</a>`);
    })

    var genres = [];
    movie.genres.forEach(function(entry) {
        genres.push(entry.name);
    })
    $('#movieModal').empty();
    $('#movieModal').append(`
        <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">${movie.title}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <div class="modal-body">
                    <img class="mx-auto d-block" src="${base_url + '/w780' + movie.backdrop_path}">
                    <p>Title: ${movie.title}</p>
                    <p>Overview: ${movie.overview}</p>
                    <p>Year: ${movie.release_date}</p>
                    <p>Genres: ${genres.join(', ')}</p>
                    <p>Director: ${directors.join(', ')}</p>
                    <p>Casts: ${casts.join(', ')}</p>
                    <p>Review:</p>
                    <div class="list-group">
                        ${htmlReviews.join('')}
                    </div>      
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
            </div>
    `);
    loading_effect('off');
    $('#movieModal').modal('show');
}

// Modal Effect
$(document).on({
    'show.bs.modal': function() {
        var zIndex = 1040 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function() {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    },
    'hidden.bs.modal': function() {
        if ($('.modal:visible').length > 0) {
            // restore the modal-open class to the body element, so that scrolling works
            // properly after de-stacking a modal.
            setTimeout(function() {
                $(document.body).addClass('modal-open');
            }, 0);
        }
    }
}, '.modal');

// Boot
$(document).ready(() => {
    load_trending();
    $('.submit_on_enter').keydown(function(event) {
        // enter has keyCode = 13, change it if you want to use another button
        if (event.keyCode == 13) {
            search();
            return false;
        }
    });
})

$('#pagination').bootpag({
    total: total_pages,
    maxVisible: 5
}).on("page", function(event, num) {
    if (curr_page === 1) {
        load_trending(num)
    } else if (curr_page === 2) {
        load_name(num)
    } else if (curr_page === 3) {
        load_actor(num)
    } else {
        event.preventDefault();
    }

});